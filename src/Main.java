public class Main {
    // Find position of an element in a sorted array of infinite numbers
    // Source: https://www.geeksforgeeks.org/find-position-element-sorted-array-infinite-numbers/
    public static void main(String[] args) {
        Integer[] arr = new Integer[1000];
        int n = 50; // We don't know number of element in array => hide
        // init array sorted ascending
        for (int i = 0; i < n; i++) {
            arr[i] = i;
        }
        int pos = linearSearch(arr, 25);
        if (pos == -1) {
            System.out.println("Not found");
        } else {
            System.out.println("Element found at: " + pos);
        }
    }

    public static int linearSearch(Integer[] arr, int value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == null)
                return -1;
            if (arr[i] == value)
                return i;
        }
        return -1;
    }

    public static int binarySearchCustom(Integer[] arr, int value) {
        int l = 0, r = arr.length - 1;
        while (l <= r) {
            int m = l + (r - l) / 2;
            if (arr[m] == null || arr[m] > value) {
                r = m - 1;
            } else if (arr[m] == value) {
                return m;
            } else {
                l = m + 1;
            }
        }
        return -1;
    }
}
